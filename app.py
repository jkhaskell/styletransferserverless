import base64
import json
import os
import uuid
from flask import Flask
from flask import jsonify
from flask import request
from neural_style import stylize
from argparse import Namespace

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World'

def generate_file_name(prefix, extension):
    session_uuid = uuid.uuid4()
    session_uuid_str = str(session_uuid)

    return f'{prefix}-{session_uuid_str}.{extension}'

@app.route('/style', methods=['POST']) 
def style():
    #get request params
    data = request.json
    encodedImage = data['image']
    model = data['model']

    #decode image and save it locally
    decodedImage = base64.b64decode(encodedImage)
    image_to_save = generate_file_name('imageToSave', 'png')
    with open(image_to_save, "wb") as fh:
        fh.write(decodedImage)

    #create the output file name
    stylized_output_file = generate_file_name('output', 'jpg')

    #prepare arguments for stylizing
    args = Namespace(content_image=image_to_save, content_scale=None, cuda=0, export_onnx=None, model='./saved_models/'+model+'.pth', output_image=stylized_output_file, subcommand='eval')
    
    #stylize!
    stylize(args)
    
    #encode new stylized image as bytes
    encoded_styled_image_bytes = 0
    
    with open(stylized_output_file, "rb") as image_file:
        encoded_styled_image_bytes = base64.b64encode(image_file.read())

    #encode new stylized image as a string
    encoded_styled_image_string = encoded_styled_image_bytes.decode('utf-8')

    #delete generated files
    os.remove(image_to_save)
    os.remove(stylized_output_file)

    #return the json payload
    return jsonify(styledImage=encoded_styled_image_string)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
